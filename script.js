const AWS = require("aws-sdk");
const fs = require("fs");
const path = require("path");
const dotenv = require("dotenv");
dotenv.config();

const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    sessionToken: process.env.AWS_SESSION_TOKEN,
    region: process.env.AWS_REGION,
    endpoint: process.env.AWS_S3_ENDPOINT,
    s3ForcePathStyle: true
});

const filename = "example.txt";
const filepath = path.join(__dirname,"results", filename);
const fileStream = fs.createReadStream(filepath);

const uploadParams = {
    Bucket: process.env.AWS_S3_BUCKETNAME,
    Body: fileStream,
    Key: 'testFolder.testFile.txt'
};

s3.upload(uploadParams,(res) =>{
    console.log(res);
});